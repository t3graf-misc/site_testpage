<?php declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SiteTestpage\Service;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Package\Event\AfterPackageActivationEvent;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * BrandingService
 */
/**
 * BrandingService
 */
class BrandingService
{
    public function __invoke(AfterPackageActivationEvent $event): void
    {
        if ($event->getPackageKey() === 'site_testpage') {
            $this->setBackendStyling();
        }
    }

    public function setBackendStyling(): void
    {
        if (class_exists(ExtensionConfiguration::class)) {
            $extensionConfiguration = GeneralUtility::makeInstance(
                ExtensionConfiguration::class
            );

            /** @var array $backendConfiguration */
            $backendConfiguration = $extensionConfiguration->get('backend');

            if (!isset($backendConfiguration['loginLogo']) || trim($backendConfiguration['loginLogo']) === '') {
                $backendConfiguration['loginLogo'] = 'EXT:site_testpage/Resources/Public/Images/Backend/login-logo.svg';
            }
            if (!isset($backendConfiguration['loginBackgroundImage']) || trim($backendConfiguration['loginBackgroundImage']) === '') {
                $backendConfiguration['loginBackgroundImage'] = 'EXT:site_testpage/Resources/Public/Images/Backend/login-background-image.jpg';
            }
            if (!isset($backendConfiguration['backendLogo']) || trim($backendConfiguration['backendLogo']) === '') {
                $backendConfiguration['backendLogo'] = 'EXT:site_testpage/Resources/Public/Images/Backend/backend-logo.svg';
            }

            // Workaround for
            // https://review.typo3.org/c/Packages/TYPO3.CMS/+/62650
            $reflection = new \ReflectionClass(ExtensionConfiguration::class);
            $parameters = $reflection->getMethod('set')->getParameters();
            if (count($parameters) === 3) {
                /** @phpstan-ignore-next-line */
                $extensionConfiguration->set('backend', '', $backendConfiguration);
            } else {
                /** @phpstan-ignore-next-line */
                $extensionConfiguration->set('backend', $backendConfiguration);
            }
        }
    }
}
